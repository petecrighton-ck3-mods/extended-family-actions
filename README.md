# Extended Family Actions

Adds several entries regarding your extended family to the Current Situation notifications.

So far included:
- Designate a guardian for every close or extended family member (instead of only your children)
- Extended family can get married (instead of only close family)
- Offer to pay the ransom when a close or extended family member is imprisoned

Should work as intended, but is not excessively tested, so please report any bugs you come across as well as feature requests: https://gitlab.com/petecrighton-ck3-mods/extended-family-actions/-/issues

English & German localisation included.

Breaks achievements, as per usual.
